import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';

import { title } from 'utils';

import { userService } from 'services';

import Article from 'components/Article';


class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            id: null,
            user: {}
        }
    }

    componentDidMount() {
        userService.list().then((list) => {
            this.setState({list})
        })
    }

    selectUser = (e) => {
        let selectedId = e.target.value;
        if (selectedId === "-1") return;
        userService.get(selectedId).then((user) => {
            this.setState(
                {user},
                () => this.props.history.push(`/users/${selectedId}`))
        })
    }

    getFormattedDate(date) {
        console.log(date)
        return new Intl.DateTimeFormat('fr-FR', {
            month: 'long',
            day: 'numeric',
            year: 'numeric'
        }).format(new Date(date));
    }


    render() {
        const { list, user } = this.state;
        return (
            <Fragment>
                <Helmet>
                    { title('Page secondaire') }
                </Helmet>

                <div className="user-page content-wrap center">
                    <Link to="/" className="nav-arrow">
                        <Icon style={{ transform: 'rotate(180deg)' }}>arrow_right_alt</Icon>
                    </Link>

                    <div className="users-select center">
                        <h1>
                            <select onChange={this.selectUser} defaultValue="-1">
                                <option value="-1">Sélectionner un utilisateur</option>
                                {list && React.Children.toArray(list.map((user) => (
                                    <option value={user.id}>{user.name}</option>
                                )))}
                            </select>
                            <Icon>arrow_drop_down</Icon>
                        </h1>
                    </div>

                    <div className="infos-block center">
                        { user.id && <Fragment>
                            <p>
                                <span className="info-label">Occupation:</span>{user.occupation}
                            </p>
                            <p>
                                <span className="info-label">Date de Naissance:</span>{this.getFormattedDate(user.birthdate)}
                            </p>
                          </Fragment>}
                    </div>

                    <div className="articles-list center">
                        {user.id && React.Children.toArray(
                            user.articles.map(({name, content}) => <Article title={name} content={content} />)
                        )}
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default UserPage;
