import React from 'react';


export default function Article({title, content}){
	return <div className="article-card">
		<h2 className="article-title">
			{title}
		</h2>
		<p className="article-content">
			{content}
		</p>
	</div>
}